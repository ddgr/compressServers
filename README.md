# compressServers

#### 项目介绍
图片批量压缩程序  
> 可以根据自己的需求批量处理图片  
> 包括质量压缩、图片截取、尺寸等比例压缩、图片旋转、添加水印等

eg：  
处理前：  
![](http://szwhg.hhhtsqzysg.cn/FileServer//BackStageWeb/UpLoad/Album/LiShi/LiShiWenHua/MingRenShengXian/BeiSongMingChen/Icon/Org/20150814030803036-source.jpg)  

处理后：  
![](http://szwhg.hhhtsqzysg.cn/FileServer//BackStageWeb/UpLoad/Album/LiShi/LiShiWenHua/MingRenShengXian/BeiSongMingChen/Icon/Org/20150814030803036.jpg)

#### 软件架构
使用了[Thumbnailator](https://github.com/DongYiBin/thumbnailator)插件   
API：[点击查看](https://coobird.github.io/thumbnailator/javadoc/0.4.8/)  
Examples：[点击查看](https://github.com/coobird/thumbnailator/wiki/Examples)

#### 使用说明

1. 源地址：需要压缩的图片路径(支持多层级文件压缩)；
2. 输出地址：存储输出文件路径(图片输出层级与源地址层级相同)，若与源地址相同则默认覆盖；
3. 图片压缩参数配置；